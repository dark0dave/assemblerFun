#!/bin/bash
# This is our as

rm *.o 2>/dev/null

ls *.asm | awk -F '.' '{print $1}' | xargs -I {} nasm -f elf64 -o {}.o -g {}.asm
# nasm -f elf64 -o CountXorSetBits.o -g CountXorSetBits.asm
# nasm -f elf64 -o Add.o -g Add.asm

gcc -c test.c

gcc -o test Sum.o test.o Add.o CountXorSetBits.o

./test
