;
; Add.asm adds two ints (32 bits) & sums an array of ints (32 bit)
;
; Parameter:	32 bit value1, 32 bit value2
; Result:		Sum of value1 and value2
;
; Called from x64 C
;
; sumOfValues = Add(int value1,int value2)

		global Add
    ;
		DEFAULT REL				; Relative Addressing
		;
		SECTION .data			; Data section
		;
		SECTION .bss
		;
		SECTION .text			; Code section.
		;
		; Macros use the following registers
		;
		; rdi - data 1 pointer
		; rsi - data 2 pointer
		; r10 - bit counter
		; r11 - data count down
		;
		; rax, rcx - scratch registers
		;

Add:
		;
		; In 64 Bit Ux only need to save
		; rbx, rbp, r12 - Not used in this function
		;
		; In 64 bit Ux the first 3 parameters are passed as:
		;
		; rdi, rsi, rdx
		;
		; Values passed as:
		; rdi = Value 1
		; rsi = Value 2
		;
		; Return Value Is In rax
		;
		mov rax,rdi					; Move value to result
    add rax,rsi         ; add second value to result
    ret
