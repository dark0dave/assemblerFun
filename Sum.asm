;
; Sum.asm takes summation of int array (32 bits)
;
; Parameter(s):	pointer to values and number of values
; Result:       summation of values
;
; Called from x64 C
;
; sumOfArray = Sum(int* data1,int countOfValues)

		global Sum
    ;
		DEFAULT REL				; Relative Addressing
		;
		SECTION .data			; Data section
		;
		SECTION .bss
		;
		SECTION .text			; Code section.
		;
		; Macros use the following registers
		;
		; rdi - data 1 pointer to array
		; rsi - countOfValues
		;
		; rax, rcx - scratch registers
		;

Sum:
		;
		; In 64 Bit Ux only need to save
		; rbx, rbp, r12 - Not used in this function
		;
		; In 64 bit Ux the first 3 parameters are passed as:
		;
		; rdi, rsi, rdx
		;
		; Values passed as:
    ;
		; rdi - data 1 pointer to array
		; rsi - countOfValues
		;
		; Return Value Is In rax
		;
    xor rax,rax         ; Clear accumlator
		mov rcx,rsi					; Move count of values to rcx as loop uses rcx to know when to stop looping

.loop:
    add rax,[rdi]       ; add second value to result, take address of rdi not
    add rdi,4           ; steps into next value adds 4 bytes to array pointer ie fetches next value
    loop .loop          ; loops over rdi until rcx is zero
    ret
