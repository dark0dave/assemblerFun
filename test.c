#include <stdio.h>

int CountXorSetBitsUx64(short* data1, short* data2, int count);
long Add(long value1, long value2);
int Sum(int* data1, int size);

int main(void) {

  short data1[1] = {0};
  short data2[1] = {0};
  int NumberOfBitsSet = CountXorSetBitsUx64(data1,data2,1);

  printf("NumberOfBitsSet = %d\n",NumberOfBitsSet);

  long value1 = 1L << 33;
  long value2 = 20;

  long sumOfValues = Add(value1,value2);

  printf("Result = %ld\n",sumOfValues);

  int data[4] = {1,2,3,4};
  int sizeOfData = 4;

  int sizeOfArray = Sum(data,sizeOfData);

  printf("Sum of array = %d\n",sizeOfArray);

  return 0;
}
